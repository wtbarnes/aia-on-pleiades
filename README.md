# aia-on-pleiades

Setup scripts and project files for analyzing AIA data on Pleiades. This is just a temporary home for a loose collection of scripts, setup files, and notebooks until more permanent infrastructure is developed.

## Logging Into Pleiades

Once you have an NAS Account and you've setup [SSH passthrough](https://www.nas.nasa.gov/hecc/support/kb/setting-up-ssh-passthrough_232.html), `ssh` into the `dfe1` node and forward the `9999` port on Pleiades back to port `8888` on your local machine,

```shell
> ssh -L 9999:127.0.0.1:8888 dfe1.nas.nasa.gov 
```

This will allow you to access the notebook in the browser on your local machine at `127.0.0.1:9999`. You can replace `dfe1` with `pfe` or `mfe1` or whatever node you're interested in using.

## Install Python and Build the Environment

Before starting up the notebook, we need to grab some packages. We'll do all of this using the [Miniconda Python distribution](https://docs.conda.io/en/latest/miniconda.html). To install Miniconda and create an environment with all of the needed dependencies,

```shell
> ./install-conda.sh
```

You'll be prompted with several questions from the Miniconda installation. The defaults should be fine. You should answer "yes" when asked if you'd like to initialize Miniconda by running conda init.

### Potential "Gotchas" 

- There may be a conflict with the version of `jupyterlab` available on `conda-forge`. If that is the case, either get `jupyterlab` from the default channel or try to downgrade it.
- If you want to use the Dask dashboard with `jupyter-server-proxy`, you may need to downgrade to version 5.7.4 of `notebook` version 5.1.1 of `tornado`.

## Starting Up the Notebook

At the command line, run the included startup script,

```shell
> ./start-jupyter.sh
```

This will load the needed modules and start the notebook at the correct port. If you want the notebook session to persist even when your ssh connection is dropped, do this in a screen session.

## Report

A preliminary report on the status of this project is also included here. If you'd like to build the PDF,

```shell
> pandoc --filter pandoc-citeproc REPORT.md -s -o REPORT.pdf
```

## Working with Reserved Nodes

For information about creating a reservation, see [this page](https://www.nas.nasa.gov/hecc/support/kb/reserving-a-dedicated-compute-node_556.html).